# SG Allow HTTP ingress access
resource "aws_security_group" "sg_allow_http" {
  name        = "sg_allow_http"
  description = "allow all ingress traffic via http"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http"
  }
}
