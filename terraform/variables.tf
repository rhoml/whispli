variable "ubuntu_ami" {
  type        = "string"
  description = "AWS EC2 AMI built with docker support"
  default     = "ami-0f870daa90c78e2c3"
}
