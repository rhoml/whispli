resource "aws_key_pair" "whispli" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDH6r061crT68EEF4euf5bBWY6tsiLNctHU/uDMu5Wdl16IzBW2oGVVY/aVOLit5sdlLPfPTXRik8l6qvWKlO3AbdAXBKexI8dl4El/4Q1ZizYEvrZnFQgn84KyhcxOX/Fre0FKFLzrk97FEP0w9JP0v4doSfQE3LCqbjYbAh1U1r2uNSdLy0+Jt4layXLg7a9e2z99ZCoJyAyBisIMf1znAuE3t6rDTEAFcNGDyrlWwkgFcecFT/91+/imConaTat4/u5fzgxrNiYSMoq2ZxlXg310/D6D90Go68rL8N8lyQ7ZKOOP8W0gLx6FyS43AfpSjk68UcQeRJiGWm8fUhoB rhoml"
}

resource "aws_instance" "web" {
  ami             = "${var.ubuntu_ami}"
  instance_type   = "t2.micro"
  security_groups = ["${aws_security_group.sg_allow_http.name}"]
  key_name        = "${aws_key_pair.whispli.key_name}"

  tags = {
    Name = "whispli"
  }
}

output "public_address" {
  value = "${aws_instance.web.public_dns}"
}
