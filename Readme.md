# Whispli

This repository contains all the resources to deploy Whispli simple API example.

# The code

The code is currently located on [Bitbucket](https://bitbucket.org/rhoml/whispli)

# Pre-requisites

To use this repository you will need to have the following tools available:

- OSX
- [Homebrew](https://brew.sh/): Package manager for OSX
- [Packer](https://www.packer.io): OOS tool for creating identical machine images.
- [Terraform])(https://www.terraform.io): OSS tool that enables you to safely and predictably create, change, and improve infrastructure.
- AWS Account and Credentials: To use this repository you will need to have a set of credentials (aws_access_key and aws_secret_key) available to use

# Usage

### Installing dependencies

All dependencies of this project can be installed using Homebrew via `brew install`

#### Packer
```
# brew install packer
# packer --version
1.4.1
```

#### Terraform
```
# brew install terraform
# terraform --version
Terraform v0.12.4
```

### Configuring your terminal

To interact with AWS you will need to configure a set of credentials (aws_access_key and aws_secret_key) and load them into your environment

```
# export AWS_ACCESS_KEY_ID=AAZVQNTEKJPIQVQGSXYI
# export AWS_SECRET_ACCESS_KEY=R7LhhUrtyUEGPlNtrjjRoBnCaU2LGtq++4YYZooA
```

### Building the code

- Clone this repository

```
# mkdir ~/myworkspace
# cd ~/myworkspace
# git clone git@bitbucket.org:rhoml/whispli.git
Cloning into 'whispli'...
remote: Counting objects: 32, done.
remote: Compressing objects: 100% (27/27), done.
remote: Total 32 (delta 4), reused 0 (delta 0)
Receiving objects: 100% (32/32), 34.99 MiB | 701.00 KiB/s, done.
Resolving deltas: 100% (4/4), done.
Checking out files: 100% (14/14), done.
# cd whispli
```

- Build the AMI

```
# cd packer
# packer build docker.json
amazon-ebs output will be in this color.

==> amazon-ebs: Prevalidating AMI Name: whispli 1563255351
    amazon-ebs: Found Image ID: ami-000c2343cf03d7fd7
==> amazon-ebs: Creating temporary keypair: packer_5d2d6237-47c4-cf68-37a6-18660bb8f44e
==> amazon-ebs: Creating temporary security group for this instance: packer_5d2d6238-a2f1-367d-683e-b113f1a4aea3
==> amazon-ebs: Authorizing access to port 22 from [0.0.0.0/0] in the temporary security groups...
==> amazon-ebs: Launching a source AWS instance...
==> amazon-ebs: Adding tags to source instance
    amazon-ebs: Adding tag: "Name": "Packer Builder"
    amazon-ebs: Instance ID: i-07648ba6511da3d81
==> amazon-ebs: Waiting for instance (i-07648ba6511da3d81) to become ready...
==> amazon-ebs: Using ssh communicator to connect: 13.238.159.76
==> amazon-ebs: Waiting for SSH to become available...
==> amazon-ebs: Connected to SSH!
==> amazon-ebs: Provisioning with shell script: scripts/dependencies.sh
==> amazon-ebs: + sudo rm -rf /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_xenial-updates_InRelease /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_xenial-updates_main_binary-amd64_Packages /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_xenial-updates_main_i18n_Translation-en /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_xenial-updates_restricted_binary-amd64_Packages /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_xenial-updates_restricted_i18n_Translation-en /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_xenial_InRelease /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_xenial_main_binary-amd64_Packages /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_xenial_main_i18n_Translation-en /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_xenial_restricted_binary-amd64_Packages /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_xenial_restricted_i18n_Translation-en /var/lib/apt/lists/lock /var/lib/apt/lists/partial /var/lib/apt/lists/security.ubuntu.com_ubuntu_dists_xenial-security_InRelease /var/lib/apt/lists/security.ubuntu.com_ubuntu_dists_xenial-security_main_binary-amd64_Packages /var/lib/apt/lists/security.ubuntu.com_ubuntu_dists_xenial-security_main_i18n_Translation-en /var/lib/apt/lists/security.ubuntu.com_ubuntu_dists_xenial-security_restricted_binary-amd64_Packages /var/lib/apt/lists/security.ubuntu.com_ubuntu_dists_xenial-security_restricted_i18n_Translation-en
==> amazon-ebs: + sudo apt-get update
    amazon-ebs: Get:1 http://security.ubuntu.com/ubuntu xenial-security InRelease [109 kB]
    amazon-ebs: Get:2 http://archive.ubuntu.com/ubuntu xenial InRelease [247 kB]
    amazon-ebs: Get:3 http://security.ubuntu.com/ubuntu xenial-security/main amd64 Packages [700 kB]
    amazon-ebs: Get:4 http://archive.ubuntu.com/ubuntu xenial-updates InRelease [109 kB]
    amazon-ebs: Get:5 http://security.ubuntu.com/ubuntu xenial-security/main Translation-en [279 kB]
    amazon-ebs: Get:6 http://security.ubuntu.com/ubuntu xenial-security/restricted amd64 Packages [7204 B]
    amazon-ebs: Get:7 http://security.ubuntu.com/ubuntu xenial-security/restricted Translation-en [2152 B]
    amazon-ebs: Get:8 http://security.ubuntu.com/ubuntu xenial-security/universe amd64 Packages [448 kB]
    amazon-ebs: Get:9 http://security.ubuntu.com/ubuntu xenial-security/universe Translation-en [182 kB]
    amazon-ebs: Get:10 http://archive.ubuntu.com/ubuntu xenial-backports InRelease [107 kB]
    amazon-ebs: Get:11 http://security.ubuntu.com/ubuntu xenial-security/multiverse amd64 Packages [5600 B]
    amazon-ebs: Get:12 http://security.ubuntu.com/ubuntu xenial-security/multiverse Translation-en [2676 B]
    amazon-ebs: Get:13 http://archive.ubuntu.com/ubuntu xenial/main amd64 Packages [1201 kB]
    amazon-ebs: Get:14 http://archive.ubuntu.com/ubuntu xenial/main Translation-en [568 kB]
    amazon-ebs: Get:15 http://archive.ubuntu.com/ubuntu xenial/restricted amd64 Packages [8344 B]
    amazon-ebs: Get:16 http://archive.ubuntu.com/ubuntu xenial/restricted Translation-en [2908 B]
    amazon-ebs: Get:17 http://archive.ubuntu.com/ubuntu xenial/universe amd64 Packages [7532 kB]
    amazon-ebs: Get:18 http://archive.ubuntu.com/ubuntu xenial/universe Translation-en [4354 kB]
    amazon-ebs: Get:19 http://archive.ubuntu.com/ubuntu xenial/multiverse amd64 Packages [144 kB]
    amazon-ebs: Get:20 http://archive.ubuntu.com/ubuntu xenial/multiverse Translation-en [106 kB]
    amazon-ebs: Get:21 http://archive.ubuntu.com/ubuntu xenial-updates/main amd64 Packages [990 kB]
    amazon-ebs: Get:22 http://archive.ubuntu.com/ubuntu xenial-updates/main Translation-en [391 kB]
    amazon-ebs: Get:23 http://archive.ubuntu.com/ubuntu xenial-updates/restricted amd64 Packages [7616 B]
    amazon-ebs: Get:24 http://archive.ubuntu.com/ubuntu xenial-updates/restricted Translation-en [2272 B]
    amazon-ebs: Get:25 http://archive.ubuntu.com/ubuntu xenial-updates/universe amd64 Packages [756 kB]
    amazon-ebs: Get:26 http://archive.ubuntu.com/ubuntu xenial-updates/universe Translation-en [315 kB]
    amazon-ebs: Get:27 http://archive.ubuntu.com/ubuntu xenial-updates/multiverse amd64 Packages [16.7 kB]
    amazon-ebs: Get:28 http://archive.ubuntu.com/ubuntu xenial-updates/multiverse Translation-en [8440 B]
    amazon-ebs: Get:29 http://archive.ubuntu.com/ubuntu xenial-backports/main amd64 Packages [7280 B]
    amazon-ebs: Get:30 http://archive.ubuntu.com/ubuntu xenial-backports/main Translation-en [4456 B]
    amazon-ebs: Get:31 http://archive.ubuntu.com/ubuntu xenial-backports/universe amd64 Packages [7804 B]
    amazon-ebs: Get:32 http://archive.ubuntu.com/ubuntu xenial-backports/universe Translation-en [4184 B]
    amazon-ebs: Fetched 18.6 MB in 10s (1718 kB/s)
    amazon-ebs: Reading package lists...
==> amazon-ebs: + sudo apt-get -y -q -o install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
==> amazon-ebs: E: Option install: Configuration item specification must have an =<val>.
==> amazon-ebs: + sudo apt-key add -
==> amazon-ebs: + curl -fsSL https://download.docker.com/linux/ubuntu/gpg
    amazon-ebs: OK
==> amazon-ebs: + sudo apt-key fingerprint 0EBFCD88
    amazon-ebs: pub   4096R/0EBFCD88 2017-02-22
    amazon-ebs:       Key fingerprint = 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
    amazon-ebs: uid                  Docker Release (CE deb) <docker@docker.com>
    amazon-ebs: sub   4096R/F273FCD8 2017-02-22
    amazon-ebs:
==> amazon-ebs: ++ lsb_release -cs
==> amazon-ebs: + sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu      xenial      stable'
==> amazon-ebs: + sudo apt-get update
    amazon-ebs: Get:1 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial InRelease [247 kB]
    amazon-ebs: Get:2 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates InRelease [109 kB]
    amazon-ebs: Get:3 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-backports InRelease [107 kB]
    amazon-ebs: Get:4 https://download.docker.com/linux/ubuntu xenial InRelease [66.2 kB]
    amazon-ebs: Get:5 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/main amd64 Packages [1,201 kB]
    amazon-ebs: Get:6 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/main Translation-en [568 kB]
    amazon-ebs: Get:7 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/restricted amd64 Packages [8,344 B]
    amazon-ebs: Get:8 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/restricted Translation-en [2,908 B]
    amazon-ebs: Get:9 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/universe amd64 Packages [7,532 kB]
    amazon-ebs: Get:10 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/universe Translation-en [4,354 kB]
    amazon-ebs: Get:11 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/multiverse amd64 Packages [144 kB]
    amazon-ebs: Get:12 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/multiverse Translation-en [106 kB]
    amazon-ebs: Get:13 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/main amd64 Packages [990 kB]
    amazon-ebs: Get:14 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/main Translation-en [391 kB]
    amazon-ebs: Get:15 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/restricted amd64 Packages [7,616 B]
    amazon-ebs: Get:16 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/restricted Translation-en [2,272 B]
    amazon-ebs: Get:17 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/universe amd64 Packages [756 kB]
    amazon-ebs: Hit:18 http://security.ubuntu.com/ubuntu xenial-security InRelease
    amazon-ebs: Get:19 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/universe Translation-en [315 kB]
    amazon-ebs: Get:20 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/multiverse amd64 Packages [16.7 kB]
    amazon-ebs: Get:21 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/multiverse Translation-en [8,440 B]
    amazon-ebs: Get:22 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-backports/main amd64 Packages [7,280 B]
    amazon-ebs: Get:23 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-backports/main Translation-en [4,456 B]
    amazon-ebs: Get:24 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-backports/universe amd64 Packages [7,804 B]
    amazon-ebs: Get:25 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-backports/universe Translation-en [4,184 B]
    amazon-ebs: Get:26 https://download.docker.com/linux/ubuntu xenial/stable amd64 Packages [9,015 B]
    amazon-ebs: Fetched 17.0 MB in 3s (5,124 kB/s)
    amazon-ebs: Reading package lists...
==> amazon-ebs: + sudo apt-get -y -q install docker-ce docker-ce-cli containerd.io
    amazon-ebs: Reading package lists...
    amazon-ebs: Building dependency tree...
    amazon-ebs: Reading state information...
    amazon-ebs: The following additional packages will be installed:
    amazon-ebs:   aufs-tools cgroupfs-mount libltdl7 pigz
    amazon-ebs: Suggested packages:
    amazon-ebs:   mountall
    amazon-ebs: The following NEW packages will be installed:
    amazon-ebs:   aufs-tools cgroupfs-mount containerd.io docker-ce docker-ce-cli libltdl7
    amazon-ebs:   pigz
    amazon-ebs: 0 upgraded, 7 newly installed, 0 to remove and 16 not upgraded.
    amazon-ebs: Need to get 53.2 MB of archives.
    amazon-ebs: After this operation, 253 MB of additional disk space will be used.
    amazon-ebs: Get:1 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/universe amd64 pigz amd64 2.3.1-2 [61.1 kB]
    amazon-ebs: Get:2 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/universe amd64 aufs-tools amd64 1:3.2+20130722-1.1ubuntu1 [92.9 kB]
    amazon-ebs: Get:3 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/universe amd64 cgroupfs-mount all 1.2 [4,970 B]
    amazon-ebs: Get:4 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/main amd64 libltdl7 amd64 2.4.6-0.1 [38.3 kB]
    amazon-ebs: Get:5 https://download.docker.com/linux/ubuntu xenial/stable amd64 containerd.io amd64 1.2.6-3 [22.6 MB]
    amazon-ebs: Get:6 https://download.docker.com/linux/ubuntu xenial/stable amd64 docker-ce-cli amd64 5:18.09.7~3-0~ubuntu-xenial [13.0 MB]
    amazon-ebs: Get:7 https://download.docker.com/linux/ubuntu xenial/stable amd64 docker-ce amd64 5:18.09.7~3-0~ubuntu-xenial [17.4 MB]
==> amazon-ebs: debconf: unable to initialize frontend: Dialog
==> amazon-ebs: debconf: (Dialog frontend will not work on a dumb terminal, an emacs shell buffer, or without a controlling terminal.)
==> amazon-ebs: debconf: falling back to frontend: Readline
==> amazon-ebs: debconf: unable to initialize frontend: Readline
==> amazon-ebs: debconf: (This frontend requires a controlling tty.)
==> amazon-ebs: debconf: falling back to frontend: Teletype
==> amazon-ebs: dpkg-preconfigure: unable to re-open stdin:
    amazon-ebs: Fetched 53.2 MB in 1s (29.8 MB/s)
    amazon-ebs: Selecting previously unselected package pigz.
    amazon-ebs: (Reading database ... 51363 files and directories currently installed.)
    amazon-ebs: Preparing to unpack .../pigz_2.3.1-2_amd64.deb ...
    amazon-ebs: Unpacking pigz (2.3.1-2) ...
    amazon-ebs: Selecting previously unselected package aufs-tools.
    amazon-ebs: Preparing to unpack .../aufs-tools_1%3a3.2+20130722-1.1ubuntu1_amd64.deb ...
    amazon-ebs: Unpacking aufs-tools (1:3.2+20130722-1.1ubuntu1) ...
    amazon-ebs: Selecting previously unselected package cgroupfs-mount.
    amazon-ebs: Preparing to unpack .../cgroupfs-mount_1.2_all.deb ...
    amazon-ebs: Unpacking cgroupfs-mount (1.2) ...
    amazon-ebs: Selecting previously unselected package containerd.io.
    amazon-ebs: Preparing to unpack .../containerd.io_1.2.6-3_amd64.deb ...
    amazon-ebs: Unpacking containerd.io (1.2.6-3) ...
    amazon-ebs: Selecting previously unselected package docker-ce-cli.
    amazon-ebs: Preparing to unpack .../docker-ce-cli_5%3a18.09.7~3-0~ubuntu-xenial_amd64.deb ...
    amazon-ebs: Unpacking docker-ce-cli (5:18.09.7~3-0~ubuntu-xenial) ...
    amazon-ebs: Selecting previously unselected package docker-ce.
    amazon-ebs: Preparing to unpack .../docker-ce_5%3a18.09.7~3-0~ubuntu-xenial_amd64.deb ...
    amazon-ebs: Unpacking docker-ce (5:18.09.7~3-0~ubuntu-xenial) ...
    amazon-ebs: Selecting previously unselected package libltdl7:amd64.
    amazon-ebs: Preparing to unpack .../libltdl7_2.4.6-0.1_amd64.deb ...
    amazon-ebs: Unpacking libltdl7:amd64 (2.4.6-0.1) ...
    amazon-ebs: Processing triggers for man-db (2.7.5-1) ...
    amazon-ebs: Processing triggers for libc-bin (2.23-0ubuntu11) ...
    amazon-ebs: Processing triggers for ureadahead (0.100.0-19.1) ...
    amazon-ebs: Processing triggers for systemd (229-4ubuntu21.21) ...
    amazon-ebs: Setting up pigz (2.3.1-2) ...
    amazon-ebs: Setting up aufs-tools (1:3.2+20130722-1.1ubuntu1) ...
    amazon-ebs: Setting up cgroupfs-mount (1.2) ...
    amazon-ebs: Setting up containerd.io (1.2.6-3) ...
    amazon-ebs: Setting up docker-ce-cli (5:18.09.7~3-0~ubuntu-xenial) ...
    amazon-ebs: Setting up docker-ce (5:18.09.7~3-0~ubuntu-xenial) ...
    amazon-ebs: update-alternatives: using /usr/bin/dockerd-ce to provide /usr/bin/dockerd (dockerd) in auto mode
    amazon-ebs: Setting up libltdl7:amd64 (2.4.6-0.1) ...
    amazon-ebs: Processing triggers for libc-bin (2.23-0ubuntu11) ...
    amazon-ebs: Processing triggers for ureadahead (0.100.0-19.1) ...
    amazon-ebs: Processing triggers for systemd (229-4ubuntu21.21) ...
==> amazon-ebs: ++ uname -s
==> amazon-ebs: ++ uname -m
==> amazon-ebs: + sudo curl -L https://github.com/docker/compose/releases/download/1.24.0/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose
==> amazon-ebs:   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
==> amazon-ebs:                                  Dload  Upload   Total   Spent    Left  Speed
==> amazon-ebs: 100   617    0   617    0     0   1564      0 --:--:-- --:--:-- --:--:--  1565
==> amazon-ebs: 100 15.4M  100 15.4M    0     0  3593k      0  0:00:04  0:00:04 --:--:-- 4214k
==> amazon-ebs: + sudo apt-get -y -q install supervisor
    amazon-ebs: Reading package lists...
    amazon-ebs: Building dependency tree...
    amazon-ebs: Reading state information...
    amazon-ebs: The following additional packages will be installed:
    amazon-ebs:   libpython-stdlib libpython2.7-minimal libpython2.7-stdlib python
    amazon-ebs:   python-meld3 python-minimal python-pkg-resources python2.7 python2.7-minimal
    amazon-ebs: Suggested packages:
    amazon-ebs:   python-doc python-tk python-setuptools python2.7-doc binutils binfmt-support
    amazon-ebs:   supervisor-doc
    amazon-ebs: The following NEW packages will be installed:
    amazon-ebs:   libpython-stdlib libpython2.7-minimal libpython2.7-stdlib python
    amazon-ebs:   python-meld3 python-minimal python-pkg-resources python2.7 python2.7-minimal
    amazon-ebs:   supervisor
    amazon-ebs: 0 upgraded, 10 newly installed, 0 to remove and 16 not upgraded.
    amazon-ebs: Need to get 4,269 kB of archives.
    amazon-ebs: After this operation, 18.6 MB of additional disk space will be used.
    amazon-ebs: Get:1 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/main amd64 libpython2.7-minimal amd64 2.7.12-1ubuntu0~16.04.4 [339 kB]
    amazon-ebs: Get:2 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/main amd64 python2.7-minimal amd64 2.7.12-1ubuntu0~16.04.4 [1,261 kB]
    amazon-ebs: Get:3 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/main amd64 python-minimal amd64 2.7.12-1~16.04 [28.1 kB]
    amazon-ebs: Get:4 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/main amd64 libpython2.7-stdlib amd64 2.7.12-1ubuntu0~16.04.4 [1,880 kB]
    amazon-ebs: Get:5 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/main amd64 python2.7 amd64 2.7.12-1ubuntu0~16.04.4 [224 kB]
    amazon-ebs: Get:6 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/main amd64 libpython-stdlib amd64 2.7.12-1~16.04 [7,768 B]
    amazon-ebs: Get:7 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/main amd64 python amd64 2.7.12-1~16.04 [137 kB]
    amazon-ebs: Get:8 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/main amd64 python-pkg-resources all 20.7.0-1 [108 kB]
    amazon-ebs: Get:9 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial/universe amd64 python-meld3 all 1.0.2-2 [30.9 kB]
    amazon-ebs: Get:10 http://ap-southeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates/universe amd64 supervisor all 3.2.0-2ubuntu0.2 [253 kB]
==> amazon-ebs: debconf: unable to initialize frontend: Dialog
==> amazon-ebs: debconf: (Dialog frontend will not work on a dumb terminal, an emacs shell buffer, or without a controlling terminal.)
==> amazon-ebs: debconf: falling back to frontend: Readline
==> amazon-ebs: debconf: unable to initialize frontend: Readline
==> amazon-ebs: debconf: (This frontend requires a controlling tty.)
==> amazon-ebs: debconf: falling back to frontend: Teletype
==> amazon-ebs: dpkg-preconfigure: unable to re-open stdin:
    amazon-ebs: Fetched 4,269 kB in 0s (44.0 MB/s)
    amazon-ebs: Selecting previously unselected package libpython2.7-minimal:amd64.
    amazon-ebs: (Reading database ... 51653 files and directories currently installed.)
    amazon-ebs: Preparing to unpack .../libpython2.7-minimal_2.7.12-1ubuntu0~16.04.4_amd64.deb ...
    amazon-ebs: Unpacking libpython2.7-minimal:amd64 (2.7.12-1ubuntu0~16.04.4) ...
    amazon-ebs: Selecting previously unselected package python2.7-minimal.
    amazon-ebs: Preparing to unpack .../python2.7-minimal_2.7.12-1ubuntu0~16.04.4_amd64.deb ...
    amazon-ebs: Unpacking python2.7-minimal (2.7.12-1ubuntu0~16.04.4) ...
    amazon-ebs: Selecting previously unselected package python-minimal.
    amazon-ebs: Preparing to unpack .../python-minimal_2.7.12-1~16.04_amd64.deb ...
    amazon-ebs: Unpacking python-minimal (2.7.12-1~16.04) ...
    amazon-ebs: Selecting previously unselected package libpython2.7-stdlib:amd64.
    amazon-ebs: Preparing to unpack .../libpython2.7-stdlib_2.7.12-1ubuntu0~16.04.4_amd64.deb ...
    amazon-ebs: Unpacking libpython2.7-stdlib:amd64 (2.7.12-1ubuntu0~16.04.4) ...
    amazon-ebs: Selecting previously unselected package python2.7.
    amazon-ebs: Preparing to unpack .../python2.7_2.7.12-1ubuntu0~16.04.4_amd64.deb ...
    amazon-ebs: Unpacking python2.7 (2.7.12-1ubuntu0~16.04.4) ...
    amazon-ebs: Selecting previously unselected package libpython-stdlib:amd64.
    amazon-ebs: Preparing to unpack .../libpython-stdlib_2.7.12-1~16.04_amd64.deb ...
    amazon-ebs: Unpacking libpython-stdlib:amd64 (2.7.12-1~16.04) ...
    amazon-ebs: Processing triggers for man-db (2.7.5-1) ...
    amazon-ebs: Processing triggers for mime-support (3.59ubuntu1) ...
    amazon-ebs: Setting up libpython2.7-minimal:amd64 (2.7.12-1ubuntu0~16.04.4) ...
    amazon-ebs: Setting up python2.7-minimal (2.7.12-1ubuntu0~16.04.4) ...
    amazon-ebs: Linking and byte-compiling packages for runtime python2.7...
    amazon-ebs: Setting up python-minimal (2.7.12-1~16.04) ...
    amazon-ebs: Selecting previously unselected package python.
    amazon-ebs: (Reading database ... 52399 files and directories currently installed.)
    amazon-ebs: Preparing to unpack .../python_2.7.12-1~16.04_amd64.deb ...
    amazon-ebs: Unpacking python (2.7.12-1~16.04) ...
    amazon-ebs: Selecting previously unselected package python-pkg-resources.
    amazon-ebs: Preparing to unpack .../python-pkg-resources_20.7.0-1_all.deb ...
    amazon-ebs: Unpacking python-pkg-resources (20.7.0-1) ...
    amazon-ebs: Selecting previously unselected package python-meld3.
    amazon-ebs: Preparing to unpack .../python-meld3_1.0.2-2_all.deb ...
    amazon-ebs: Unpacking python-meld3 (1.0.2-2) ...
    amazon-ebs: Selecting previously unselected package supervisor.
    amazon-ebs: Preparing to unpack .../supervisor_3.2.0-2ubuntu0.2_all.deb ...
    amazon-ebs: Unpacking supervisor (3.2.0-2ubuntu0.2) ...
    amazon-ebs: Processing triggers for man-db (2.7.5-1) ...
    amazon-ebs: Processing triggers for ureadahead (0.100.0-19.1) ...
    amazon-ebs: Processing triggers for systemd (229-4ubuntu21.21) ...
    amazon-ebs: Setting up libpython2.7-stdlib:amd64 (2.7.12-1ubuntu0~16.04.4) ...
    amazon-ebs: Setting up python2.7 (2.7.12-1ubuntu0~16.04.4) ...
    amazon-ebs: Setting up libpython-stdlib:amd64 (2.7.12-1~16.04) ...
    amazon-ebs: Setting up python (2.7.12-1~16.04) ...
    amazon-ebs: Setting up python-pkg-resources (20.7.0-1) ...
    amazon-ebs: Setting up python-meld3 (1.0.2-2) ...
    amazon-ebs: Setting up supervisor (3.2.0-2ubuntu0.2) ...
    amazon-ebs: Processing triggers for ureadahead (0.100.0-19.1) ...
    amazon-ebs: Processing triggers for systemd (229-4ubuntu21.21) ...
==> amazon-ebs: + sudo chmod +x /usr/local/bin/docker-compose
==> amazon-ebs: + sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
==> amazon-ebs: + sudo mkdir -p /docker-compose
==> amazon-ebs: + sudo docker pull mysql:5.6
    amazon-ebs: 5.6: Pulling from library/mysql
    amazon-ebs: fc7181108d40: Pulling fs layer
    amazon-ebs: 787a24c80112: Pulling fs layer
    amazon-ebs: a08cb039d3cd: Pulling fs layer
    amazon-ebs: 4f7d35eb5394: Pulling fs layer
    amazon-ebs: 5aa21f895d95: Pulling fs layer
    amazon-ebs: 345649b63bc3: Pulling fs layer
    amazon-ebs: 591a87fc59ec: Pulling fs layer
    amazon-ebs: 6019ecdb5901: Pulling fs layer
    amazon-ebs: aadc682b6a75: Pulling fs layer
    amazon-ebs: e0969183ab5f: Pulling fs layer
    amazon-ebs: 8a01a78fff5a: Pulling fs layer
    amazon-ebs: 4f7d35eb5394: Waiting
    amazon-ebs: 5aa21f895d95: Waiting
    amazon-ebs: 345649b63bc3: Waiting
    amazon-ebs: 591a87fc59ec: Waiting
    amazon-ebs: 6019ecdb5901: Waiting
    amazon-ebs: aadc682b6a75: Waiting
    amazon-ebs: e0969183ab5f: Waiting
    amazon-ebs: 8a01a78fff5a: Waiting
    amazon-ebs: 787a24c80112: Verifying Checksum
    amazon-ebs: 787a24c80112: Download complete
    amazon-ebs: fc7181108d40: Verifying Checksum
    amazon-ebs: fc7181108d40: Download complete
    amazon-ebs: a08cb039d3cd: Verifying Checksum
    amazon-ebs: a08cb039d3cd: Download complete
    amazon-ebs: 4f7d35eb5394: Verifying Checksum
    amazon-ebs: 4f7d35eb5394: Download complete
    amazon-ebs: 5aa21f895d95: Verifying Checksum
    amazon-ebs: 5aa21f895d95: Download complete
    amazon-ebs: 345649b63bc3: Verifying Checksum
    amazon-ebs: 345649b63bc3: Download complete
    amazon-ebs: fc7181108d40: Pull complete
    amazon-ebs: 787a24c80112: Pull complete
    amazon-ebs: 591a87fc59ec: Verifying Checksum
    amazon-ebs: 591a87fc59ec: Download complete
    amazon-ebs: 6019ecdb5901: Verifying Checksum
    amazon-ebs: 6019ecdb5901: Download complete
    amazon-ebs: aadc682b6a75: Verifying Checksum
    amazon-ebs: aadc682b6a75: Download complete
    amazon-ebs: a08cb039d3cd: Pull complete
    amazon-ebs: 4f7d35eb5394: Pull complete
    amazon-ebs: 5aa21f895d95: Pull complete
    amazon-ebs: e0969183ab5f: Verifying Checksum
    amazon-ebs: e0969183ab5f: Download complete
    amazon-ebs: 8a01a78fff5a: Verifying Checksum
    amazon-ebs: 8a01a78fff5a: Download complete
    amazon-ebs: 345649b63bc3: Pull complete
    amazon-ebs: 591a87fc59ec: Pull complete
    amazon-ebs: 6019ecdb5901: Pull complete
    amazon-ebs: aadc682b6a75: Pull complete
    amazon-ebs: e0969183ab5f: Pull complete
    amazon-ebs: 8a01a78fff5a: Pull complete
    amazon-ebs: Digest: sha256:e4a70c0f52bfda9ed28d2127b461ff44cbc381450e7ca22e83936560d8875f14
    amazon-ebs: Status: Downloaded newer image for mysql:5.6
==> amazon-ebs: + sudo docker pull whispli/devops-challenge-api
    amazon-ebs: Using default tag: latest
    amazon-ebs: latest: Pulling from whispli/devops-challenge-api
    amazon-ebs: c5e155d5a1d1: Pulling fs layer
    amazon-ebs: 221d80d00ae9: Pulling fs layer
    amazon-ebs: 4250b3117dca: Pulling fs layer
    amazon-ebs: 3b7ca19181b2: Pulling fs layer
    amazon-ebs: 425d7b2a5bcc: Pulling fs layer
    amazon-ebs: 69df12c70287: Pulling fs layer
    amazon-ebs: 2a68245de447: Pulling fs layer
    amazon-ebs: 4f61e9705839: Pulling fs layer
    amazon-ebs: 574f196ee25a: Pulling fs layer
    amazon-ebs: cac0e6a96aa6: Pulling fs layer
    amazon-ebs: ec31cb1fbdb2: Pulling fs layer
    amazon-ebs: 10026d3aae36: Pulling fs layer
    amazon-ebs: 3b7ca19181b2: Waiting
    amazon-ebs: 425d7b2a5bcc: Waiting
    amazon-ebs: 69df12c70287: Waiting
    amazon-ebs: 2a68245de447: Waiting
    amazon-ebs: 4f61e9705839: Waiting
    amazon-ebs: 574f196ee25a: Waiting
    amazon-ebs: cac0e6a96aa6: Waiting
    amazon-ebs: ec31cb1fbdb2: Waiting
    amazon-ebs: 10026d3aae36: Waiting
    amazon-ebs: 4250b3117dca: Verifying Checksum
    amazon-ebs: 4250b3117dca: Download complete
    amazon-ebs: 221d80d00ae9: Verifying Checksum
    amazon-ebs: 221d80d00ae9: Download complete
    amazon-ebs: c5e155d5a1d1: Verifying Checksum
    amazon-ebs: c5e155d5a1d1: Download complete
    amazon-ebs: 3b7ca19181b2: Verifying Checksum
    amazon-ebs: 3b7ca19181b2: Download complete
    amazon-ebs: 69df12c70287: Verifying Checksum
    amazon-ebs: 69df12c70287: Download complete
    amazon-ebs: 4f61e9705839: Verifying Checksum
    amazon-ebs: 4f61e9705839: Download complete
    amazon-ebs: 2a68245de447: Verifying Checksum
    amazon-ebs: 2a68245de447: Download complete
    amazon-ebs: 425d7b2a5bcc: Verifying Checksum
    amazon-ebs: 425d7b2a5bcc: Download complete
    amazon-ebs: 574f196ee25a: Verifying Checksum
    amazon-ebs: 574f196ee25a: Download complete
    amazon-ebs: cac0e6a96aa6: Verifying Checksum
    amazon-ebs: cac0e6a96aa6: Download complete
    amazon-ebs: ec31cb1fbdb2: Verifying Checksum
    amazon-ebs: ec31cb1fbdb2: Download complete
    amazon-ebs: 10026d3aae36: Verifying Checksum
    amazon-ebs: 10026d3aae36: Download complete
    amazon-ebs: c5e155d5a1d1: Pull complete
    amazon-ebs: 221d80d00ae9: Pull complete
    amazon-ebs: 4250b3117dca: Pull complete
    amazon-ebs: 3b7ca19181b2: Pull complete
    amazon-ebs: 425d7b2a5bcc: Pull complete
    amazon-ebs: 69df12c70287: Pull complete
    amazon-ebs: 2a68245de447: Pull complete
    amazon-ebs: 4f61e9705839: Pull complete
    amazon-ebs: 574f196ee25a: Pull complete
    amazon-ebs: cac0e6a96aa6: Pull complete
    amazon-ebs: ec31cb1fbdb2: Pull complete
    amazon-ebs: 10026d3aae36: Pull complete
    amazon-ebs: Digest: sha256:ed6a529fd82874539e9c825057143de2ea001ad35ad41b2445280368485c7444
    amazon-ebs: Status: Downloaded newer image for whispli/devops-challenge-api:latest
==> amazon-ebs: Uploading files/supervisord.conf => /tmp/supervisord.conf

supervisord.conf 2.89 KiB / 2.89 KiB [===================================================================================================================================] 100.00% 0s

supervisord.conf 2.89 KiB / 2.89 KiB [===================================================================================================================================] 100.00% 0s
==> amazon-ebs: Uploading files/docker-compose.yml => /tmp/docker-compose.yml

docker-compose.yml 549 B / 549 B [=======================================================================================================================================] 100.00% 0s

docker-compose.yml 549 B / 549 B [=======================================================================================================================================] 100.00% 0s
==> amazon-ebs: Provisioning with shell script: scripts/recall_files.sh
==> amazon-ebs: Stopping the source instance...
    amazon-ebs: Stopping instance
==> amazon-ebs: Waiting for the instance to stop...
==> amazon-ebs: Creating AMI whispli 1563255351 from instance i-07648ba6511da3d81
    amazon-ebs: AMI: ami-0c6201272015b04ad
==> amazon-ebs: Waiting for AMI to become ready...
==> amazon-ebs: Terminating the source AWS instance...
==> amazon-ebs: Cleaning up any extra volumes...
==> amazon-ebs: No volumes to clean up, skipping
==> amazon-ebs: Deleting temporary security group...
==> amazon-ebs: Deleting temporary keypair...
Build 'amazon-ebs' finished.

==> Builds finished. The artifacts of successful builds are:
--> amazon-ebs: AMIs were created:
ap-southeast-2: ami-0c6201272015b04ad
```

- Provision the new AMI on AWS

The first step is the initialization of our repository. Terraform requires to download the modules used by our provisioners:

```
# cd ~/myworkspace/whispli/terraform
# terraform init

Initializing the backend...

Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "aws" (terraform-providers/aws) 2.19.0...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.aws: version = "~> 2.19"

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

Once initialized we can proceed to ru na terraform plan

```
# terraform plan -var 'ubuntu_ami=ami-0c6201272015b04ad' -no-color
Refreshing Terraform state in-memory prior to plan...
The refreshed state will be used to calculate this plan, but will not be
persisted to local or remote state storage.


------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.web will be created
  + resource "aws_instance" "web" {
      + ami                          = "ami-0c6201272015b04ad"
      + arn                          = (known after apply)
      + associate_public_ip_address  = (known after apply)
      + availability_zone            = (known after apply)
      + cpu_core_count               = (known after apply)
      + cpu_threads_per_core         = (known after apply)
      + get_password_data            = false
      + host_id                      = (known after apply)
      + id                           = (known after apply)
      + instance_state               = (known after apply)
      + instance_type                = "t2.micro"
      + ipv6_address_count           = (known after apply)
      + ipv6_addresses               = (known after apply)
      + key_name                     = "deployer-key"
      + network_interface_id         = (known after apply)
      + password_data                = (known after apply)
      + placement_group              = (known after apply)
      + primary_network_interface_id = (known after apply)
      + private_dns                  = (known after apply)
      + private_ip                   = (known after apply)
      + public_dns                   = (known after apply)
      + public_ip                    = (known after apply)
      + security_groups              = [
          + "sg_allow_http",
        ]
      + source_dest_check            = true
      + subnet_id                    = (known after apply)
      + tags                         = {
          + "Name" = "whispli"
        }
      + tenancy                      = (known after apply)
      + volume_tags                  = (known after apply)
      + vpc_security_group_ids       = (known after apply)

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + snapshot_id           = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = (known after apply)
          + iops                  = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

  # aws_key_pair.whispli will be created
  + resource "aws_key_pair" "whispli" {
      + fingerprint = (known after apply)
      + id          = (known after apply)
      + key_name    = "deployer-key"
      + public_key  = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDH6r061crT68EEF4euf5bBWY6tsiLNctHU/uDMu5Wdl16IzBW2oGVVY/aVOLit5sdlLPfPTXRik8l6qvWKlO3AbdAXBKexI8dl4El/4Q1ZizYEvrZnFQgn84KyhcxOX/Fre0FKFLzrk97FEP0w9JP0v4doSfQE3LCqbjYbAh1U1r2uNSdLy0+Jt4layXLg7a9e2z99ZCoJyAyBisIMf1znAuE3t6rDTEAFcNGDyrlWwkgFcecFT/91+/imConaTat4/u5fzgxrNiYSMoq2ZxlXg310/D6D90Go68rL8N8lyQ7ZKOOP8W0gLx6FyS43AfpSjk68UcQeRJiGWm8fUhoB rhoml"
    }

  # aws_security_group.sg_allow_http will be created
  + resource "aws_security_group" "sg_allow_http" {
      + arn                    = (known after apply)
      + description            = "allow all ingress traffic via http"
      + egress                 = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 0
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "-1"
              + security_groups  = []
              + self             = false
              + to_port          = 0
            },
        ]
      + id                     = (known after apply)
      + ingress                = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 80
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 80
            },
        ]
      + name                   = "sg_allow_http"
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags                   = {
          + "Name" = "allow_http"
        }
      + vpc_id                 = (known after apply)
    }

Plan: 3 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------

Note: You didn't specify an "-out" parameter to save this plan, so Terraform
can't guarantee that exactly these actions will be performed if
"terraform apply" is subsequently run.

```

- After validating our changes we can proceed and apply such changes

```
# terraform apply -var 'ubuntu_ami=ami-0c6201272015b04ad' -no-color

aws_key_pair.whispli: Creating...
aws_security_group.sg_allow_http: Creating...
aws_key_pair.whispli: Creation complete after 0s [id=deployer-key]
aws_security_group.sg_allow_http: Creation complete after 2s [id=sg-02598b306e9d2788f]
aws_instance.web: Creating...
aws_instance.web: Still creating... [10s elapsed]
aws_instance.web: Still creating... [20s elapsed]
aws_instance.web: Still creating... [30s elapsed]
aws_instance.web: Creation complete after 32s [id=i-06cce9c3e96bc4c2e]

Apply complete! Resources: 3 added, 0 changed, 0 destroyed.

Outputs:

public_address = ec2-13-54-20-187.ap-southeast-2.compute.amazonaws.com
```

- Open your browser pointing at ec2-13-54-20-187.ap-southeast-2.compute.amazonaws.com and the result must be similar to ![](images/capture.png)

# Suggested improvements

Here is a list of suggestions that can be done to this project to improve its quality, given the right amount of time:

- CI/CD pipeline: Bitbucket offers the possibility to use Bitbucket pipelines, that means that using a yml file we could build and deploy the resources to AWS programatically using proper workflows.
- Use of ECS / Fargate: I was playing with the idea of using fargate to deploy these containers. Given its low cost and mainteance seemed like a good idea. However, setting all the requisites in terraform would take some extra effort and time which I am currently lacking.
- Terraform remote state: Given that this is a one man operation and I am using the free tier of AWS on my personal account I decided to keep it simple. In an ideal scenario, we would configure the remote state to live in S3 and locking mechanisms using DynamoDB. These are safety measures in case the repote state is corrupted or we have parallel terraform applies.
- RDS: If this is a production grade application I wouldn't deploy MySQL using docker containers. The ideal scenario would be to deploy the webapp on EC2/ECS/EKS/ElasticBeanstalk and the database on RDS with multi-az to ensure its availability and backups.
- Reliability: Once we separate the database from the application, the next step is to use ELBs. This will allow us to horizontally scale our application.
- Security: TLS! as we have an application exposed to the internet we would like to encrypt the traffic. Once again we can use the ELBs to terminate TLS. If cost is an issue we could use Let's encrypt.
- [Inspect](https://www.inspec.io/docs/): For ensuring compliance, in an ideal scenario you would build you AMIs and run inpect tests to ensure that your AMIs are compliant. Specially when running mission critical applications.


# Next steps

I am leaving the instance up and running until EOW then it will be automatically terminated.

# Ackknowledges

I also want to take sometime to thank all the whispli team for the time that you took to interview me both in person and reviewing this exercise. 

It was really fun to refresh my memory on how packer and docker-compose worked. Haven't done that for almost a year.
